﻿//th{box-sizing:border-box} - всегда

; (function ($, _) {
	'use strict';

	var registeredTables = {};

	var defaults = {
		selector: '>thead>tr>th',
		store: {
			get: $.noop,
			set: $.noop
		},

		beforeStartCallback: $.noop,
		afterStartCallback: $.noop,

		beforeEndResizeCallback: $.noop,
		afterEndReizeCallback: $.noop,

		fixedWidths: {},
		minColumnWidth: 20,
		minColumnWidths: {},
		useBody: false
	}

	var Api = {
		init: function (registeredTable) {
			//create data-fixed-visible attributes
			var $table = registeredTable.$table;
			var $ths = $table.find(registeredTable.selector);

			_.forEach($ths, function (th) {
				th = $(th);
				if (!th.attr('data-column-name')) {
					th.attr('data-column-name', _.uniqueId('column-'));
				}
			});

			//create data-fixed-width attributes			
			_.forEach($ths, function (th) {
				th = $(th);
				var fixedWidth = registeredTable.fixedWidths[th.attr('data-column-name')];
				if (_.isNumber(fixedWidth) || parseFloat(fixedWidth)) {
					th.attr('data-fixed-width', fixedWidth);
				}
			});

			//set widths
			var widths = {};

			var $fixedThs = $ths.filter('[data-fixed-width]');
			_.forEach($fixedThs, function (th) {
				th = $(th);
				var columnName = th.attr('data-column-name');
				var fixedWidth = registeredTable.fixedWidths[columnName];

				if (_.isString(fixedWidth) && parseFloat(fixedWidth)) {
					fixedWidth = $table.width() * parseFloat(fixedWidth) / 100;
				}

				widths[columnName] = fixedWidth;
			});

			var $nonFixedThs = $ths.filter(':not([data-fixed-width])');
			_.forEach($nonFixedThs, function (th) {
				th = $(th);
				var columnName = th.attr('data-column-name');

				var width = registeredTable.store.get(columnName);
				if (!width) {
					width = th.outerWidth();
				}

				var minWidth = Api.getMinColumnWidth(registeredTable, columnName);
				widths[columnName] = width > minWidth ? width : minWidth;
			});

			_.forEach($ths, function (th) {
				th = $(th);
				var columnName = th.attr('data-column-name');
				th.css('width', widths[columnName]);
			});

			//Нужно для упращения работы с видимотью.
			//Если не сбросить в 0 то, когда размер таблицы больше, чем количество колонок,
			//chrome растягивает все колонки пропорционально, ie вообще странно себя ведет
			$table.addClass('resizeble-table');

			//add resizers
			_.forEach($nonFixedThs, function (th) {
				th = $(th);
				var resizer = $('<div>');
				var resizerCid = _.uniqueId('resizer-');
				registeredTable.resizers[resizerCid] = resizer;
				var columnName = th.attr('data-column-name');

				resizer.addClass('resizer').on('mousedown', function (e) {
					registeredTable.beforeStartCallback(th, columnName, registeredTable.store);
					registeredTable.$table.trigger('resize:start:before', th, columnName, registeredTable.store);

					registeredTable.state.nowResizeColumnName = columnName;
					registeredTable.state.pageX = e.pageX;

					registeredTable.afterStartCallback(th, columnName, registeredTable.store);
					registeredTable.$table.trigger('resize:start', columnName, registeredTable.store);

					if (!registeredTable.cursorStyle) {
						var cursorStyle = $('<style>').html('*{ cursor: e-resize !important }');
						$('body').append(cursorStyle)
						registeredTable.cursorStyle = cursorStyle;
					}

				}).attr('data-resizer-cid', resizerCid);


				$(window).on('mouseup.' + resizerCid, function (e) {
					if (columnName != registeredTable.state.nowResizeColumnName) return;

					registeredTable.beforeEndResizeCallback(th, columnName, registeredTable.store);
					registeredTable.$table.trigger('resize:end:before', th, columnName, registeredTable.store);

					registeredTable.store.set(columnName, registeredTable.state.widths[columnName]);
					registeredTable.state.nowResizeColumnName = null;
					e.pageX = null;
					registeredTable.cursorStyle.remove();
					registeredTable.cursorStyle = null;

					registeredTable.afterStartCallback(th, columnName, registeredTable.store);
					registeredTable.$table.trigger('resize:end', th, columnName, registeredTable.store);
				});
				th.append(resizer);
			});

			registeredTable.state.widths = widths;
			//----------------------------------------------------------------


			$(window).on('mousemove.' + registeredTable.cid, function (e) {
				var columnName = registeredTable.state.nowResizeColumnName;
				if (!columnName) return;

				var widths = registeredTable.state.widths;
				var delta = e.pageX - registeredTable.state.pageX;
				var minWidth = Api.getMinColumnWidth(registeredTable, columnName);

				if (widths[columnName] + delta >= minWidth) {
					widths[columnName] += delta;
					$ths.filter('[data-column-name=' + columnName + ']').outerWidth(widths[columnName]);
					registeredTable.state.pageX = e.pageX;
				}
			});
		},

		getMinColumnWidth: function (rt, columnName) {
			var minWidth = rt.minColumnWidths[columnName] ? rt.minColumnWidths[columnName] : rt.minColumnWidth;
			return minWidth;
		},

		restoreState: function (cid) {
			var registeredTable = registeredTables[cid];
			if (!registeredTable) throw 'table does not regiser';

			var $table = registeredTable.$table;
			var options = {
				store: registeredTable.store,
				selector: registeredTable.selector,
				fixedWidths: registeredTable.fixedWidths,
				minColumnWidth: registeredTable.minColumnWidth,

				beforeStartCallback: registeredTable.beforeStartCallback,
				afterStartCallback: registeredTable.afterStartCallback,

				beforeEndResizeCallback: registeredTable.beforeEndResizeCallback,
				afterEndReizeCallback: registeredTable.afterEndReizeCallback,
			};

			this.unregisterTable(cid);
			var registeredTable = this.registerTable($table, options, cid);
			this.init(registeredTable);
		},

		registerTable: function ($table, options, existingCid) {
			var cid = existingCid || _.uniqueId('resizeble-table-');
			registeredTables[cid] = {
				$table: $table,
				store: options.store,
				selector: options.selector,
				fixedWidths: options.fixedWidths,

				minColumnWidth: options.minColumnWidth,
				minColumnWidths: options.minColumnWidths,

				beforeStartCallback: options.beforeStartCallback,
				afterStartCallback: options.afterStartCallback,

				beforeEndResizeCallback: options.beforeEndResizeCallback,
				afterEndReizeCallback: options.afterEndReizeCallback,

				resizers: {},
				state: {},
				cid: cid
			}
			$table.attr('data-resizebleCid', cid);

			return registeredTables[cid];
		},

		unregisterTable: function (cid) {
			var registeredTable = registeredTables[cid];
			if (!registeredTable) return;

			_.forEach(registeredTable.resizers, function ($resizer, resizerCid) {
				$(window).off('mouseup.' + resizerCid);
				$resizer.remove();
			});
			$(window).off('mousemove.' + registeredTable.cid);
			registeredTable.$table.removeClass('resizeble-table')
			delete registeredTables[cid];
		}
	}


	$.fn.resizeTableColumns = function (operation, options) {
		var cid = this.attr('data-resizebleCid');
		if (operation == 'init') {
			if (cid) return this;
			_.defaults(options, defaults);
			var registeredTable = Api.registerTable(this, options);
			Api.init(registeredTable, defaults);
			return this;
		}

		if (operation == 'sync') {
			Api.restoreState(cid);
			return this;
		}

		if (operation == 'close') {
			Api.unregisterTable(cid);
			return this;
		}
	}

})(jQuery, _);